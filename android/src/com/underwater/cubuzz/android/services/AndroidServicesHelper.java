package com.underwater.cubuzz.android.services;

import android.content.Intent;
import com.google.GameHelper;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.underwater.cubuzz.android.AndroidLauncher;
import com.underwater.cubuzz.android.R;
import com.underwater.cubuzz.services.ServicesHelper;

/**
 * Created by Gev on 6/29/2015.
 */
public class AndroidServicesHelper implements ServicesHelper, GameHelper.GameHelperListener {

    private static final int RC_UNUSED = 5001;

    private final GameHelper mHelper;
    private AndroidLauncher androidLancher;


    public AndroidServicesHelper(AndroidLauncher androidLuncher) {
        this.androidLancher = androidLuncher;
        mHelper = new GameHelper(androidLuncher, GameHelper.CLIENT_GAMES);
        mHelper.enableDebugLog(true);
        mHelper.setup(this);
        mHelper.beginUserInitiatedSignIn();
    }

    protected GoogleApiClient getApiClient() {
        return mHelper.getApiClient();
    }

    protected boolean isSignedIn() {
        return mHelper.isSignedIn();
    }

    @Override
    public void submitToLeaderboard(final int score) {
        androidLancher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Games.Leaderboards.submitScore(getApiClient(), androidLancher.getString(R.string.leaderboard_id), score);
            }
        });

    }

    @Override
    public void showLeaderboard() {
        androidLancher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isSignedIn()) {
                    androidLancher.startActivityForResult(Games.Leaderboards.getAllLeaderboardsIntent(getApiClient()),
                            RC_UNUSED);
                }
            }
        });

    }

    public void onStart() {
        mHelper.onStart(androidLancher);
    }

    public void onStop() {
        mHelper.onStop();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onSignInFailed() {

    }

    @Override
    public void onSignInSucceeded() {

    }



}

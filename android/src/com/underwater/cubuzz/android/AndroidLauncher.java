package com.underwater.cubuzz.android;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.underwater.cubuzz.Cubuzz;
import com.underwater.cubuzz.android.ad.AndroidAdNetwork;
import com.underwater.cubuzz.android.billing.AndroidStore;
import com.underwater.cubuzz.android.interaction.AndroidUserInteraction;
import com.underwater.cubuzz.android.notification.NotificationService;
import com.underwater.cubuzz.android.services.AndroidServicesHelper;

public class AndroidLauncher extends AndroidApplication {
	private static final String LICENSE_KEY = "";

	private Cubuzz game;
	private AndroidStore androidStore;
	private AndroidAdNetwork adNetwork;
	private AndroidServicesHelper serviceHelper;

	public RelativeLayout mLayout;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//set orientation
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		//init main layout
		mLayout = new RelativeLayout(this);

		//init game
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		config.useImmersiveMode = true;
		game = new Cubuzz();
		View gameView = initializeForView(game, config);

		//add game to main layout
		mLayout.addView(gameView);

		//init store
		androidStore = new AndroidStore(this, LICENSE_KEY);
		game.setStore(androidStore);

		//init adnetwork
		adNetwork = new AndroidAdNetwork(this);
		game.setAdNetwork(adNetwork);

		//init user interaction interface
		game.setInteractionInterface(new AndroidUserInteraction(this));

		//init Android service interface
		serviceHelper = new AndroidServicesHelper(this);
		game.setServicesHelper(serviceHelper);

		setContentView(mLayout);

		stopService(new Intent(this, NotificationService.class));
	}

	@Override
	protected void onDestroy() {
		if (androidStore != null) {
			androidStore.release();
			androidStore = null;
		}
		adNetwork.onDestroy();
		super.onDestroy();
	}

	@Override
	protected void onStart() {
		super.onStart();
		serviceHelper.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
		serviceHelper.onStop();
	}

	@Override
	protected void onPause() {
		super.onPause();
		startService(new Intent(this, NotificationService.class));
	}

	@Override
	protected void onResume() {
		super.onResume();
		stopService(new Intent(this, NotificationService.class));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (!androidStore.handleActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		}
		serviceHelper.onActivityResult(requestCode, resultCode, data);
	}
}

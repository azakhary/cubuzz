package com.underwater.cubuzz.android.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.underwater.cubuzz.android.AndroidLauncher;
import com.underwater.cubuzz.android.R;

/**
 * Created by Gev on 7/2/2015.
 */
public class NotificationService extends Service {

    private final static String TAG = "NotificationService";

    public static final String COUNTDOWN_BR = "com.underwater.cubuzz.android";
    Intent bi = new Intent(COUNTDOWN_BR);

    private CountDownTimer[] timers = new CountDownTimer[5];

    private int ONE_DAY_INTERVAL = 86400000;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "Starting timer...");
        if(timers != null){
            cancelAllTimers();
        }

        int startingTime = ONE_DAY_INTERVAL;

        for(int i = 0; i < 5; i++) {
            createTimedNotification(i, startingTime);
            int randomShiftTime = (int) (Math.random() * 10000 * 7200000);
            startingTime+=ONE_DAY_INTERVAL+randomShiftTime;
        }

    }

    private void createTimedNotification(int index, int time) {
        CountDownTimer cdt = new CountDownTimer(time,time) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                this.cancel();

                Intent notificationIntent = new Intent(NotificationService.this.getApplicationContext(), AndroidLauncher.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent intent = PendingIntent.getActivity(NotificationService.this.getApplicationContext(), 0, notificationIntent, 0);

                NotificationCompat.Builder mBuilder = new NotificationCompat
                        .Builder(NotificationService.this.getApplicationContext())
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("My notification")
                        .setContentText("Hello World!")
                        .setContentIntent(intent);

                Notification notification = mBuilder.build();
                notification.flags |= Notification.FLAG_AUTO_CANCEL;

                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                // mId allows you to update the notification later on.
                mNotificationManager.notify(0, notification);
            }
        };

        cdt.start();
        timers[index] = cdt;
    }

    @Override
    public boolean stopService(Intent name) {
        cancelAllTimers();
        return super.stopService(name);

    }

    @Override
    public void onDestroy() {
        cancelAllTimers();
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    private void cancelAllTimers() {
        for(int i = 0; i < timers.length; i++) {
            if( timers[i] != null) {
                timers[i].cancel();
            }
        }
    }
}

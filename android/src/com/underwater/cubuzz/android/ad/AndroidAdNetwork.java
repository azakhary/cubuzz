package com.underwater.cubuzz.android.ad;

import android.view.View;
import android.widget.RelativeLayout;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Timer;
import com.google.android.gms.ads.*;
import com.underwater.cubuzz.Cubuzz;
import com.underwater.cubuzz.ad.AdNetwork;
import com.underwater.cubuzz.android.AndroidLauncher;

/**
 * Created by Eduard on 2/4/2015.
 */
public class AndroidAdNetwork implements AdNetwork {
    private static final String ADVIEW_UNIT_ID = "ca-app-pub-7260969235130910/4774024593";
    private static final String INTERSTITIALAD_UNIT_ID = "ca-app-pub-7260969235130910/1140728193";

    private final AndroidLauncher androidLauncher;
    private AdView adView;
    private InterstitialAd interstitial;
    private Timer timer;

    private boolean isFirstTime;

    public AndroidAdNetwork(AndroidLauncher androidLauncher) {
        this.androidLauncher = androidLauncher;
        initInterstitial();
        initBanner();
    }

    private void initInterstitial() {
        Preferences prefs = Gdx.app.getPreferences("isFirstTime");
        isFirstTime = prefs.getBoolean("isFirstTime", true);

        if(isFirstTime) {
            prefs.putBoolean("isFirstTime", false);
            prefs.flush();
            System.out.println("adMob first time");
        }
        if(!isFirstTime) {
            System.out.println("adMob not first time");
            interstitial = new InterstitialAd(androidLauncher);
            interstitial.setAdUnitId(INTERSTITIALAD_UNIT_ID);
            interstitial.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    if (interstitial.isLoaded()) {
                        System.out.println("adMob loaded");
                        interstitial.show();
                        System.out.println("adMob shown");
                    }
                }
            });
        }

    }

    private void initBanner() {
        androidLauncher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adView = new AdView(androidLauncher);
                adView.setAdSize(AdSize.SMART_BANNER);
                adView.setAdUnitId(ADVIEW_UNIT_ID);
                AdRequest adRequest = new AdRequest.Builder()
                        .addTestDevice("4E0E5A26A2DC045C1F88792B653840EA")
                        .build();
                adView.loadAd(adRequest);
                RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                //adParams.addRule(RelativeLayout.ABOVE);
                adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
                adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                androidLauncher.mLayout.addView(adView, adParams);
                hideBanner();
            }
        });
    }

    @Override
    public void showBanner() {
        androidLauncher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Preferences prefs = Gdx.app.getPreferences(Cubuzz.PREFS);
                boolean noAds = prefs.getBoolean(Cubuzz.PREFS_NO_ADS,false);
                if(noAds){
                    hideBanner();
                    return;
                }
                //System.out.println("show AD banner");
                adView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void hideBanner() {
        androidLauncher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void showInterstitial() {
        Preferences prefs = Gdx.app.getPreferences(Cubuzz.PREFS);
        boolean noAds = prefs.getBoolean(Cubuzz.PREFS_NO_ADS,false);
        if(noAds){
            return;
        }
        if(isFirstTime) return;

        androidLauncher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Create ad request.
                AdRequest adRequest = new AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .addTestDevice("4E0E5A26A2DC045C1F88792B653840EA")
                        .build();
                // Begin loading your interstitial.
                interstitial.loadAd(adRequest);
                System.out.println("adMob loading");
            }
        });
    }

    public AdView getAdView() {
        return adView;
    }

    public void onDestroy() {
        interstitial = null;
        hideBanner();
        adView.destroy();
    }


}


package com.underwater.cubuzz.android.interaction;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;
import com.badlogic.gdx.files.FileHandle;
import com.underwater.cubuzz.android.AndroidLauncher;
import com.underwater.cubuzz.interaction.UserInteraction;

import java.io.File;

/**
 * Created by Gev on 6/25/2015.
 */
public class AndroidUserInteraction implements UserInteraction {
    AndroidLauncher androidLauncher;
    public AndroidUserInteraction(AndroidLauncher androidLuncher) {
        this.androidLauncher = androidLuncher;
    }

    @Override
    public void takeToMarket(){
        androidLauncher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=com.underwater.cubuzz"));
                androidLauncher.startActivity(intent);
            }
        });

    }

    @Override
    public void sendEmail(final FileHandle fileHandle) {
        if(fileHandle == null){
            return;
        }
        androidLauncher.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("message/rfc822");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"email@example.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "subject here");
                intent.putExtra(Intent.EXTRA_TEXT, "body text");
                File root = getCacheDir();
                File file = new File(root, fileHandle.name());
                if (!file.exists() || !file.canRead()) {
                    Toast.makeText(androidLauncher, "Attachment Error", Toast.LENGTH_SHORT).show();
                    return;
                }
                Uri uri = Uri.fromFile(file);
                intent.putExtra(Intent.EXTRA_STREAM, uri);
                androidLauncher.startActivity(Intent.createChooser(intent, "Send email..."));
            }
        });

    }

    @Override
    public void openTextBook() {
        String id = "u5CVsCnxyXg";
        try{
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + id));
            androidLauncher.startActivity(intent);
        }catch (ActivityNotFoundException ex){
            Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v="+id));
            androidLauncher.startActivity(intent);
        }
    }

    @Override
    public File getCacheDir() {
        return Environment.getExternalStorageDirectory();
    }
}

package com.underwater.cubuzz.services;

/**
 * Google Play services wrapping interface.
 * Created by Gev on 6/29/2015.
 */
public interface ServicesHelper {
    void submitToLeaderboard(int score);
    void showLeaderboard();
}

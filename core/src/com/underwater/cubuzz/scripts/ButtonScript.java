package com.underwater.cubuzz.scripts;

import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.script.SimpleButtonScript;

/**
 * Simple Button (extended for some possible additional functionality)
 * Created by azakhary on 6/23/2015.
 */
public class ButtonScript extends SimpleButtonScript {

    public static ButtonScript selfInit(CompositeItem item) {
        ButtonScript script = new ButtonScript();
        item.addScript(script);

        return script;
    }
}

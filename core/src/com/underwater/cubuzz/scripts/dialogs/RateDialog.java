package com.underwater.cubuzz.scripts.dialogs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.underwater.cubuzz.Cubuzz;
import com.underwater.cubuzz.MainStage;
import com.underwater.cubuzz.scripts.ButtonScript;
import com.underwater.cubuzz.scripts.DialogScript;
import com.uwsoft.editor.renderer.actor.CompositeItem;

/**
 * "Rate us" dialog logic
 * Created by azakhary on 6/26/2015.
 */
public class RateDialog extends DialogScript {

    private ButtonScript cancelBtn;
    private ButtonScript laterBtn;
    private ButtonScript rateBtn;

    public RateDialog(MainStage stage) {
        super(stage);
    }

    @Override
    public void init(CompositeItem item) {
        super.init(item);

        cancelBtn = ButtonScript.selfInit(composite.getCompositeById("cancelBtn"));
        laterBtn = ButtonScript.selfInit(composite.getCompositeById("laterBtn"));
        rateBtn = ButtonScript.selfInit(composite.getCompositeById("rateBtn"));

        final Preferences prefs = Gdx.app.getPreferences(Cubuzz.PREFS);

        cancelBtn.addListener(new ClickListener() {
            @Override
            public void clicked (InputEvent event, float x, float y) {
                hide();
                prefs.putBoolean(Cubuzz.PREFS_SHOW_RATE_US, false);
                prefs.flush();
            }
        });

        laterBtn.addListener(new ClickListener() {
            @Override
            public void clicked (InputEvent event, float x, float y) {
                hide();
            }
        });

        rateBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                hide();
                prefs.putBoolean(Cubuzz.PREFS_SHOW_RATE_US, false);
                prefs.flush();
                stage.app.getUserInteraction().takeToMarket();
            }
        });
    }
}

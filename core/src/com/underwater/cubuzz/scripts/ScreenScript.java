package com.underwater.cubuzz.scripts;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.underwater.cubuzz.MainStage;
import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.script.IScript;
import com.uwsoft.editor.renderer.utils.CustomVariables;

import java.io.SequenceInputStream;

/**
 * Created by azakhary on 6/23/2015.
 */
public class ScreenScript implements IScript {

    protected CompositeItem composite;
    protected MainStage stage;

    public static final float slideTime = 0.2f;

    public ScreenScript(MainStage stage) {
        this.stage = stage;
    }

    @Override
    public void init(CompositeItem item) {
        composite = item;
        composite.setTouchable(Touchable.disabled);
        composite.setVisible(false);
    }

    @Override
    public void act(float delta) {

    }

    public void show() {
        composite.setTouchable(Touchable.enabled);
        composite.setVisible(true);

        CustomVariables vars = composite.getCustomVariables();
        Vector2 offset = new Vector2(vars.getFloatVariable("xOffset"), vars.getFloatVariable("yOffset"));
        offset.x *= composite.mulX;
        offset.y *= composite.mulY;
        Action slideIn = Actions.moveTo(offset.x, stage.getHeight() - offset.y - composite.getHeight(), slideTime);
        composite.setPosition(stage.getWidth() + offset.x, stage.getHeight() - offset.y - composite.getHeight());
        composite.addAction(slideIn);
    }

    public void hide() {
        CustomVariables vars = composite.getCustomVariables();
        Vector2 offset = new Vector2(vars.getFloatVariable("xOffset"), vars.getFloatVariable("yOffset"));
        offset.x *= composite.mulX;
        offset.y *= composite.mulY;
        Action slideIn = Actions.moveTo(offset.x - stage.getWidth(), stage.getHeight() - offset.y - composite.getHeight(), slideTime);
        composite.setPosition(offset.x, stage.getHeight() - offset.y - composite.getHeight());
        composite.addAction(Actions.sequence(slideIn, Actions.run(new Runnable() {
            @Override
            public void run() {
                composite.setTouchable(Touchable.disabled);
                composite.setVisible(false);
            }
        })));
    }

    public CompositeItem getComposite() {
        return composite;
    }

    @Override
    public void dispose() {

    }

}

package com.underwater.cubuzz.scripts.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.underwater.cubuzz.Cubuzz;
import com.underwater.cubuzz.MainStage;
import com.underwater.cubuzz.utils.PhysicsDrag;
import com.underwater.cubuzz.utils.ScreenshotFactory;
import com.underwater.cubuzz.ad.AdNetwork;
import com.underwater.cubuzz.scripts.ButtonScript;
import com.underwater.cubuzz.scripts.ScreenScript;
import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.actor.ImageItem;

/**
 * Created by azakhary on 6/23/2015.
 */
public class GameScreen extends ScreenScript {

    private ImageItem fieldItem;

    private ButtonScript menuBtn;
    private ButtonScript resultsBtn;

    private World world;
    private float timeAcc = 0;
    private Body itemBody;
    private PhysicsDrag finger;

    private static final float WORLD_SCALE = 1/10f;
    private static final float dumping = 0.8f;

    private boolean gameStarted = false;

    private int hits = 0;

    private Preferences prefs;

    public GameScreen(MainStage stage) {
        super(stage);
        world = new World(new Vector2(0, 0), true);

        prefs = Gdx.app.getPreferences(Cubuzz.PREFS);
    }

    /**
     * Initializing game once when app starts, creating walls and initialising World
     * @param item
     */
    @Override
    public void init(CompositeItem item) {
        super.init(item);

        menuBtn = ButtonScript.selfInit(composite.getCompositeById("menuBtn"));
        resultsBtn = ButtonScript.selfInit(composite.getCompositeById("resultsBtn"));

        menuBtn.addListener(new ClickListener() {
            @Override
            public void clicked (InputEvent event, float x, float y) {
                stopGame();
                stage.showScreen(stage.menuScreen);

            }
        });

        resultsBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                stage.app.getServicesHelper().showLeaderboard();
            }
        });

        fieldItem = composite.getImageById("field");
        finger = new PhysicsDrag(world);

        createWalls();
        addListeners();
    }

    /**
     * Adding main physics listeners, and physics joint mouse listeners
     */
    private void addListeners() {
        world.setContactListener(new ContactListener() {

            @Override
            public void beginContact(Contact contact) {

            }

            @Override
            public void endContact(Contact contact) {
                if (gameStarted) {
                    hits++;
                    updateHitsLabel();
                }
            }

            @Override
            public void preSolve(Contact contact, Manifold oldManifold) {

            }

            @Override
            public void postSolve(Contact contact, ContactImpulse impulse) {

            }
        });

        // add global listener on this screen
        composite.addListener(new InputListener() {
            Vector2 target = new Vector2();

            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);

                if (!gameStarted) {
                    finger.draggingImage = checkIntersect(x, y);
                    if (finger.draggingImage != null) {
                        MouseJointDef def = new MouseJointDef();
                        def.bodyA = finger.groundBody;
                        def.bodyB = itemBody;
                        def.collideConnected = true;
                        def.target.set(x * WORLD_SCALE, y * WORLD_SCALE);
                        def.maxForce = 4000.0f * itemBody.getMass();

                        finger.mouseJoint = (MouseJoint) (world.createJoint(def));
                        itemBody.setAwake(true);
                        itemBody.setAngularDamping(10f);
                    }
                }

                return true;
            }

            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                super.touchDragged(event, x, y, pointer);
                if (finger.draggingImage != null) {
                    finger.mouseJoint.setTarget(target.set(x * WORLD_SCALE, y * WORLD_SCALE));
                }
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                if (finger.draggingImage != null) {
                    itemBody.setAngularDamping(0.8f);
                    world.destroyJoint(finger.mouseJoint);
                    finger.mouseJoint = null;
                    finger.draggingImage = null;
                    gameStarted = true;
                }
            }
        });
    }

    private void updateHitsLabel() {
        composite.getLabelById("scoreLbl").setText(hits + "");
    }

    /**
     * Resets game to initial status
     */
    private void resetGame() {
        stopGame();
        gameStarted = false;
        hits = 0;
        updateHitsLabel();
        itemBody = createBox();

        prefs.putBoolean(Cubuzz.PREFS_GAME_IN_PROGRESS, true);
        prefs.flush();
    }

    /**
     * Create all 4 bounding walls
     */
    private void createWalls() {
        float thickness = 5f * fieldItem.mulX;

        createWall(fieldItem.getX() + fieldItem.getWidth() / 2, fieldItem.getY() - thickness, fieldItem.getWidth() / 2, thickness);
        createWall(fieldItem.getX() + fieldItem.getWidth() / 2, fieldItem.getY() + fieldItem.getHeight() + thickness, fieldItem.getWidth() / 2, thickness);

        createWall(fieldItem.getX() - thickness, fieldItem.getY() + fieldItem.getHeight() / 2, thickness, fieldItem.getHeight() / 2);
        createWall(fieldItem.getX() + fieldItem.getWidth() + thickness, fieldItem.getY() + fieldItem.getHeight() / 2, thickness, fieldItem.getHeight() / 2);
    }

    /**
     * Method to create one wall
     * @param x
     * @param y
     * @param width
     * @param height
     */
    private void createWall(float x, float y, float width, float height) {
        FixtureDef fixtureDef = new FixtureDef();

        fixtureDef.density = 1f;
        fixtureDef.friction = 1.0f;
        fixtureDef.restitution = 0.7f;

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(x * WORLD_SCALE, y * WORLD_SCALE);

        bodyDef.type = BodyDef.BodyType.StaticBody;

        Body body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(width * WORLD_SCALE, height * WORLD_SCALE);

        fixtureDef.shape = polygonShape;
        body.createFixture(fixtureDef);
    }

    /**
     * Creates the main box physics data and view (currently nine patch item, but you can change it to anything)
     * @return
     */
    private Body createBox() {
        TextureAtlas.AtlasRegion region = (TextureAtlas.AtlasRegion) stage.app.getRm().getTextureRegion("item");
        Image image = new Image(new NinePatch(region, region.splits[0], region.splits[1], region.splits[2], region.splits[3]));
        image.setColor(114f / 255, 247f / 255f, 142f / 255f, 0.8f);
        image.setWidth(210 * composite.mulX);
        image.setHeight(210 * composite.mulY);
        Group imageContainer = new Group();
        imageContainer.setWidth(image.getWidth());
        imageContainer.setHeight(image.getHeight());
        imageContainer.setOrigin(image.getWidth()/2f, image.getHeight()/2f);
        imageContainer.addActor(image);
        composite.addActor(imageContainer);

        Vector2 position = new Vector2();
        position.x = fieldItem.getX() + fieldItem.getWidth() / 2 - imageContainer.getWidth() / 2;
        position.y = fieldItem.getY() + fieldItem.getHeight() / 2 - imageContainer.getHeight() / 2;

        FixtureDef fixtureDef = new FixtureDef();

        fixtureDef.density = 20f;
        fixtureDef.friction = 6.0f;
        fixtureDef.restitution = 0.1f;

        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set((position.x+image.getWidth()/2f) * WORLD_SCALE, (position.y+image.getHeight()/2f) * WORLD_SCALE);

        bodyDef.type = BodyDef.BodyType.DynamicBody;

        Body body = world.createBody(bodyDef);

        PolygonShape polygonShape = new PolygonShape();
        polygonShape.setAsBox(image.getWidth() / 2f * WORLD_SCALE, image.getHeight() / 2f * WORLD_SCALE);

        fixtureDef.shape = polygonShape;
        body.createFixture(fixtureDef);


        body.setUserData(imageContainer);
        body.setLinearDamping(dumping);

        return body;
    }

    @Override
    public void show() {
        super.show();
        resetGame();
    }


    @Override
    public void act(float delta) {
        super.act(delta);
        if(itemBody != null) {
            worldTick(delta);
            if(itemBody.isAwake()) {
                Group box = (Group) itemBody.getUserData();
                box.setPosition((itemBody.getPosition().x / WORLD_SCALE) - box.getWidth() / 2, (itemBody.getPosition().y / WORLD_SCALE) - box.getHeight() / 2);
                box.setRotation(itemBody.getAngle() * MathUtils.radiansToDegrees);
             } else if(!itemBody.isAwake() && gameStarted){
                gameEnded();
            }
        }
    }

    private void gameEnded() {
        Pixmap scr = ScreenshotFactory.getScreenshot();
        stage.gameOverScreen.setScreenshot(scr);
        stopGame();
        stage.gameOverScreen.setNewScore(hits);
        stage.showScreen(stage.gameOverScreen);

        AdNetwork adNetwork = stage.app.getAdNetwork();
        if(adNetwork != null) {
            adNetwork.showInterstitial();
        }

        prefs.putBoolean(Cubuzz.PREFS_GAME_IN_PROGRESS, false);
        prefs.flush();
    }

    private void stopGame() {
        if(itemBody != null) {
            Group group = (Group) itemBody.getUserData();
            group.remove();
            world.destroyBody(itemBody);
            itemBody = null;
        }
    }

    private void worldTick(float delta) {
        while (timeAcc < delta) {
            timeAcc += 1f/60;
            world.step(1f/60, 10, 10);
        }
        timeAcc -= delta;
    }

    /**
     * Checks if mouse click intersects with the main box (so that we can drag it later)
     * @param x
     * @param y
     * @return
     */
    public Group checkIntersect(float x, float y) {
        if(itemBody == null) return null;

        Group box = (Group) itemBody.getUserData();

        Vector2 point = new Vector2(x, y);
        Rectangle imgRect = new Rectangle(box.getX(), box.getY(), box.getWidth(), box.getHeight());

        if(imgRect.contains(point)) {
            return box;
        }

        return null;
    }
}

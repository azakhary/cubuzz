package com.underwater.cubuzz.scripts.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.underwater.cubuzz.Cubuzz;
import com.underwater.cubuzz.MainStage;
import com.underwater.cubuzz.ad.AdNetwork;
import com.underwater.cubuzz.billing.Store;
import com.underwater.cubuzz.billing.StoreListenerAdapter;
import com.underwater.cubuzz.billing.product.Product;
import com.underwater.cubuzz.scripts.ButtonScript;
import com.underwater.cubuzz.scripts.ScreenScript;
import com.uwsoft.editor.renderer.actor.CompositeItem;

/**
 * Created by azakhary on 6/23/2015.
 */
public class MenuScreen extends ScreenScript {

    private ButtonScript soundBtn;

    private ButtonScript continueBtn;
    private ButtonScript newGameBtn;
    private ButtonScript textBookBtn;
    private ButtonScript removeAdsBtn;
    private ButtonScript restorePurchasesBtn;

    public MenuScreen(MainStage stage) {
        super(stage);
        AdNetwork adNetwork = stage.app.getAdNetwork();
        Store store = stage.app.getStore();
        if(adNetwork != null) {
            adNetwork.hideBanner();
        }
        if(store != null) {
            store.addListener(new StoreListenerAdapter());
        }
    }

    @Override
    public void show() {
        super.show();
        Preferences prefs = Gdx.app.getPreferences(Cubuzz.PREFS);
        boolean isGameInProgress = prefs.getBoolean(Cubuzz.PREFS_GAME_IN_PROGRESS, false);

        if(isGameInProgress) {
            //show continue button
            showContinueButton();
        } else {
            // hide continue button
            removeContinueButton();
        }
    }

    @Override
    public void init(CompositeItem item) {
        super.init(item);

        soundBtn = ButtonScript.selfInit(composite.getCompositeById("soundBtn"));

        continueBtn = ButtonScript.selfInit(composite.getCompositeById("continueBtn"));
        newGameBtn = ButtonScript.selfInit(composite.getCompositeById("newGameBtn"));
        textBookBtn = ButtonScript.selfInit(composite.getCompositeById("textBookBtn"));
        removeAdsBtn = ButtonScript.selfInit(composite.getCompositeById("removeAdsBtn"));
        restorePurchasesBtn = ButtonScript.selfInit(composite.getCompositeById("restorePurchasesBtn"));

        final Preferences prefs = Gdx.app.getPreferences(Cubuzz.PREFS);
        boolean isSoundOn = prefs.getBoolean(Cubuzz.PREFS_SOUND_STATE_KEY, false);
        soundBtn.setToggle(isSoundOn);

        soundBtn.addListener(new ClickListener() {
            @Override
            public void clicked (InputEvent event, float x, float y) {
                prefs.putBoolean(Cubuzz.PREFS_SOUND_STATE_KEY, soundBtn.isToggled());
                prefs.flush();
            }
        });

        continueBtn.addListener(new ClickListener() {
            @Override
            public void clicked (InputEvent event, float x, float y) {
                stage.showScreen(stage.gameScreen);
                AdNetwork adNetwork = stage.app.getAdNetwork();
                if(adNetwork != null) {
                    adNetwork.showBanner();
                }
            }
        });

        newGameBtn.addListener(new ClickListener() {
            @Override
            public void clicked (InputEvent event, float x, float y) {
                stage.showScreen(stage.gameScreen);

                AdNetwork adNetwork = stage.app.getAdNetwork();
                if(adNetwork != null) {
                    adNetwork.showBanner();
                }
            }
        });

        textBookBtn.addListener(new ClickListener() {
            @Override
            public void clicked (InputEvent event, float x, float y) {
                stage.app.getUserInteraction().openTextBook();
            }
        });

        removeAdsBtn.addListener(new ClickListener() {
            @Override
            public void clicked (InputEvent event, float x, float y) {
                Product product = stage.app.getStore().getProductById(Cubuzz.NO_ADS_PRODUCT);
                stage.app.getStore().purchaseProduct(product.id);
            }
        });

        restorePurchasesBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                stage.app.getStore().restoreTransactions();
            }
        });
    }

    private void removeContinueButton() {
        continueBtn.getItem().setVisible(false);
        continueBtn.getItem().setTouchable(Touchable.disabled);

        float offset = continueBtn.getItem().getY() - newGameBtn.getItem().getY();

        newGameBtn.getItem().setY(newGameBtn.getItem().getY() + offset);
        textBookBtn.getItem().setY(textBookBtn.getItem().getY() + offset);
        removeAdsBtn.getItem().setY(removeAdsBtn.getItem().getY() + offset);
        restorePurchasesBtn.getItem().setY(restorePurchasesBtn.getItem().getY() + offset);
    }

    private void showContinueButton() {
        continueBtn.getItem().setVisible(true);
        continueBtn.getItem().setTouchable(Touchable.enabled);

        if(newGameBtn.getItem().getY() == continueBtn.getItem().getY()) {
            float offset = newGameBtn.getItem().getY() - textBookBtn.getItem().getY();

            newGameBtn.getItem().setY(newGameBtn.getItem().getY() - offset);
            textBookBtn.getItem().setY(textBookBtn.getItem().getY() - offset);
            removeAdsBtn.getItem().setY(removeAdsBtn.getItem().getY() - offset);
            restorePurchasesBtn.getItem().setY(restorePurchasesBtn.getItem().getY() - offset);
        }
    }
}

package com.underwater.cubuzz.scripts.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.underwater.cubuzz.Cubuzz;
import com.underwater.cubuzz.MainStage;
import com.underwater.cubuzz.utils.ScreenshotFactory;
import com.underwater.cubuzz.scripts.ButtonScript;
import com.underwater.cubuzz.scripts.ScreenScript;
import com.uwsoft.editor.renderer.actor.CompositeItem;

/**
 * Created by azakhary on 6/23/2015.
 */
public class GameOverScreen extends ScreenScript {

    private ButtonScript newGameBtn;
    private ButtonScript menuBtn;
    private ButtonScript rateBtn;
    private ButtonScript shareBtn;

    private Pixmap screenshot;

    private Preferences prefs;
    private int runCounter;

    public GameOverScreen(MainStage stage) {
        super(stage);
    }

    @Override
    public void init(CompositeItem item) {
        super.init(item);

        newGameBtn = ButtonScript.selfInit(composite.getCompositeById("newGameBtn"));
        menuBtn = ButtonScript.selfInit(composite.getCompositeById("menuBtn"));
        rateBtn = ButtonScript.selfInit(composite.getCompositeById("rateBtn"));
        shareBtn = ButtonScript.selfInit(composite.getCompositeById("shareBtn"));

        newGameBtn.addListener(new ClickListener() {
            @Override
            public void clicked (InputEvent event, float x, float y) {
                stage.showScreen(stage.gameScreen);
            }
        });

        menuBtn.addListener(new ClickListener() {
            @Override
            public void clicked (InputEvent event, float x, float y) {
                stage.showScreen(stage.menuScreen);
            }
        });

        rateBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                stage.app.getUserInteraction().takeToMarket();
            }
        });

        shareBtn.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                FileHandle fileHandle = ScreenshotFactory.saveScreenshot(stage.app.getTmpFile(), screenshot);
                stage.app.getUserInteraction().sendEmail(fileHandle);
            }
        });

        prefs = Gdx.app.getPreferences(Cubuzz.PREFS);
    }

    @Override
    public void show() {
        super.show();

        // check if it's time to show "rate us" dialog
        runCounter = prefs.getInteger(Cubuzz.PREFS_GAME_RUN_COUNTER, 0);
        boolean showRateDialog = prefs.getBoolean(Cubuzz.PREFS_SHOW_RATE_US, true);
        runCounter++;
        prefs.putInteger(Cubuzz.PREFS_GAME_RUN_COUNTER, runCounter);
        prefs.flush();

        if(showRateDialog && runCounter % Cubuzz.RUNS_PER_RATING_REQUEST == 0) {
            stage.rateDialog.show();
        }
    }

    public void setNewScore(int hits) {
        Integer maxScore = prefs.getInteger(Cubuzz.PREFS_MAX_SCORE_KEY, 0);

        if(maxScore < hits) {
            maxScore = hits;
            prefs.putInteger(Cubuzz.PREFS_MAX_SCORE_KEY, maxScore);
            prefs.flush();
            stage.app.getServicesHelper().submitToLeaderboard(maxScore);
        }

        composite.getLabelById("currScoreLbl").setText(hits + "");
        composite.getLabelById("bestScoreLbl").setText(maxScore + "");
    }

    public void setScreenshot(Pixmap screenshot) {
        if(this.screenshot != null) this.screenshot.dispose();
        this.screenshot = screenshot;
    }
}

package com.underwater.cubuzz.scripts;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.underwater.cubuzz.MainStage;
import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.script.IScript;

/**
 * Created by azakhary on 6/26/2015.
 */
public class DialogScript implements IScript {

    protected CompositeItem composite;
    protected MainStage stage;

    public DialogScript(MainStage stage) {
        this.stage = stage;
    }

    @Override
    public void init(CompositeItem item) {
        composite = item;
        composite.setTouchable(Touchable.disabled);
        composite.setVisible(false);
    }

    public void show() {
        composite.setTouchable(Touchable.enabled);
        composite.setVisible(true);
        composite.getColor().a = 0;

        composite.setPosition(stage.getWidth()/2 - composite.getWidth()/2, stage.getHeight()/2 - composite.getHeight()/2);

        Action fadeIn = Actions.fadeIn(0.3f);
        composite.addAction(fadeIn);

        // fade out bg
        stage.getCurrentScreen().getComposite().addAction(Actions.alpha(0.3f, 0.2f));
    }

    public void hide() {

        Action fadeOut = Actions.fadeOut(0.3f);
        composite.addAction(Actions.sequence(fadeOut, Actions.run(new Runnable() {
            @Override
            public void run() {
                composite.setTouchable(Touchable.disabled);
                composite.setVisible(false);
                composite.setPosition(stage.getWidth() / 2 - composite.getWidth() / 2, -composite.getHeight() / 2);
            }
        })));

        // show bg back
        stage.getCurrentScreen().getComposite().addAction(Actions.alpha(1f, 0.2f));
    }

    @Override
    public void act(float delta) {

    }

    @Override
    public void dispose() {

    }
}

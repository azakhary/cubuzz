package com.underwater.cubuzz.interaction;

import com.badlogic.gdx.files.FileHandle;

import java.io.File;

/**
 * Created by Gev on 6/25/2015.
 */
public interface UserInteraction {
    void takeToMarket();
    void sendEmail(FileHandle fileHandle);
    void openTextBook();
    File getCacheDir();
}

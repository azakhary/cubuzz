package com.underwater.cubuzz.i18n;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.I18NBundle;
import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.actor.IBaseItem;
import com.uwsoft.editor.renderer.actor.LabelItem;

import java.util.*;


/**
 * Language manager for i18n standard
 *
 * Created by azakhary on 6/24/2015.
 */
public class LanguageManager {
    public static final String LANGUAGE_EN = "en";
    public static final String LANGUAGE_RU = "ru";
    //
    private static HashMap<String, Locale> SUPPORTED_LANGUAGES = new HashMap<String, Locale>() {{
        put(LANGUAGE_EN, Locale.US);
        put(LANGUAGE_RU, new Locale("ru"));
    }};

    private I18NBundle i18NBundle;
    private String currentLanguage;

    public LanguageManager() {

    }
    public  void setCurrentLanguage(String lang){
        currentLanguage = lang;
        if (!isCurrentLanguageSupported()) {
            currentLanguage = LANGUAGE_EN;
        }
        loadLanguage(currentLanguage, Gdx.files.internal("i18n/strings"));
    }

    private void loadLanguage(String name, FileHandle fileHandle) {
        if (name != null && !name.isEmpty() && fileHandle != null)
            i18NBundle = I18NBundle.createBundle(fileHandle,SUPPORTED_LANGUAGES.get(name));
    }

    private boolean isCurrentLanguageSupported() {
        Iterator it = SUPPORTED_LANGUAGES.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if (pair.getKey().equals(currentLanguage)) {
                return true;
            }
        }
        return false;
    }

    public void localize(CompositeItem root) {
        ArrayList<IBaseItem> items = root.getItems();
        for(IBaseItem item: items) {
            if(item.isComposite()) {
                localize((CompositeItem) item);
            }
            if(item instanceof LabelItem) {
                LabelItem label = (LabelItem) item;
                String key = "overlap2d." + label.getDataVO().itemIdentifier;
                if(keyExist(key)) {
                    label.setText(localize(key));
                }
            }
        }
    }

    public void localize(LabelItem labelItem, String key, Object... args) {
        labelItem.setText(i18NBundle.format(key, args));
    }

    public String localize(String key) {
        return i18NBundle.format(key);
    }

    public boolean keyExist(String key) {
        try {
            String value = i18NBundle.get(key);
        } catch (MissingResourceException exception) {
            return false;
        }

        return true;
    }

    public String localize(String key, Object... args) {
        return i18NBundle.format(key, args);
    }
    public String getCurrentLanguage(){
        return currentLanguage;
    }
}
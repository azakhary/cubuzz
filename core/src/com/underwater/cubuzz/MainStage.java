package com.underwater.cubuzz;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.underwater.cubuzz.ad.AdNetwork;
import com.underwater.cubuzz.billing.Store;
import com.underwater.cubuzz.scripts.dialogs.RateDialog;
import com.underwater.cubuzz.scripts.screens.GameScreen;
import com.underwater.cubuzz.scripts.screens.MenuScreen;
import com.underwater.cubuzz.scripts.screens.GameOverScreen;
import com.underwater.cubuzz.scripts.ScreenScript;
import com.underwater.cubuzz.utils.ItemUtils;
import com.uwsoft.editor.renderer.SceneLoader;
/**
 * Main game logic goes here, this is a Stage instance that holds all the screens and actors.
 * Created by azakhary on 6/23/2015.
 */
public class MainStage extends Stage {

    public Cubuzz app;
    private SceneLoader sl;

    // Holding the current displaying screen (only one screen can be displayed at a time)
    private ScreenScript currentScreen;

    // Game as 3 screen, all 3 are preloaded in memory for super quick access
    public MenuScreen menuScreen;
    public GameScreen gameScreen;
    public GameOverScreen gameOverScreen;

    // list of dialogs ready to be shown
    public RateDialog rateDialog;

    public MainStage(Cubuzz app) {
        super(new ScalingViewport(Scaling.stretch, app.resolution.width, (float) app.resolution.width / (float) Gdx.graphics.getWidth() * Gdx.graphics.getHeight(), new OrthographicCamera()));

        // Making sure stage listens to input events.
        Gdx.input.setInputProcessor(this);

        this.app = app;

        // Loading data from overlap2d
        sl = new SceneLoader(app.getRm());
        sl.setResolution(app.resolution.name);
        sl.loadScene("MainScene");
        addActor(sl.getRoot());

        // Localizing all texts
        app.languageManager.localize(sl.getRoot());
        ItemUtils.roundCoordinates(sl.getRoot());

        // init screen logic classes
        menuScreen = new MenuScreen(this);
        gameScreen = new GameScreen(this);
        gameOverScreen = new GameOverScreen(this);
        rateDialog = new RateDialog(this);

        // Init Screen scripts for each screen
        sl.getRoot().getCompositeById("mainMenuScreen").addScript(menuScreen);
        sl.getRoot().getCompositeById("gameScreen").addScript(gameScreen);
        sl.getRoot().getCompositeById("gameOverScreen").addScript(gameOverScreen);
        sl.getRoot().getCompositeById("rateDlg").addScript(rateDialog);

        // Starting with displaying menu screen
        showScreen(menuScreen);

        // Hiding the game banner as it is only shown in game screen
        AdNetwork adNetwork = app.getAdNetwork();
        if(adNetwork != null) {
            adNetwork.hideBanner();
        }
    }

    /**
     * This is utility method to swap screens
     * @param screen
     */
    public void showScreen(ScreenScript screen) {
        if(currentScreen != null) {
            currentScreen.hide();
        }

        currentScreen = screen;

        screen.show();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
    }

    public ScreenScript getCurrentScreen() {
        return currentScreen;
    }
}

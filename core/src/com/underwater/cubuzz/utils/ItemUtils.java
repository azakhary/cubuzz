package com.underwater.cubuzz.utils;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.uwsoft.editor.renderer.actor.CompositeItem;
import com.uwsoft.editor.renderer.actor.IBaseItem;
import com.uwsoft.editor.renderer.actor.LabelItem;

import java.util.ArrayList;

/**
 * Created by azakhary on 6/24/2015.
 */
public class ItemUtils {

    public static void roundCoordinates(CompositeItem root) {
        ArrayList<IBaseItem> items = root.getItems();
        for(IBaseItem item: items) {
            if(item.isComposite()) {
                roundCoordinates((CompositeItem) item);
            }
            Actor actor = (Actor) item;
            actor.setX(Math.round(actor.getX()));
            actor.setY(Math.round(actor.getY()));
        }
    }
}

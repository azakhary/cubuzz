package com.underwater.cubuzz.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.utils.ScreenUtils;

import java.nio.ByteBuffer;

/**
 * Utility class for capturing game screenshot
 * Created by azakhary on 6/24/2015.
 */
public class ScreenshotFactory {

    public static FileHandle saveScreenshot() {
        Pixmap pixmap = getScreenshot();
        try{
            String screenshotName = "tmpCubbuzScr.png";
            FileHandle fh = new FileHandle(screenshotName);
            PixmapIO.writePNG(fh, pixmap);
            pixmap.dispose();
            return fh;
        }catch (Exception e){
        }

        return null;
    }

    public static FileHandle saveScreenshot(FileHandle fh, Pixmap pixmap){
        try{
            PixmapIO.writePNG(fh, pixmap);
            pixmap.dispose();
            return fh;
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

        return fh;
    }

    public static Pixmap getScreenshot(){
        return getScreenshot(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
    }

    public static Pixmap getScreenshot(int x, int y, int w, int h, boolean yDown){
        final Pixmap pixmap = ScreenUtils.getFrameBufferPixmap(x, y, w, h);

        if (yDown) {
            // Flip the pixmap upside down
            ByteBuffer pixels = pixmap.getPixels();
            int numBytes = w * h * 4;
            byte[] lines = new byte[numBytes];
            int numBytesPerLine = w * 4;
            for (int i = 0; i < h; i++) {
                pixels.position((h - i - 1) * numBytesPerLine);
                pixels.get(lines, i * numBytesPerLine, numBytesPerLine);
            }
            pixels.clear();
            pixels.put(lines);
        }

        return pixmap;
    }
}
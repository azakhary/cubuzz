package com.underwater.cubuzz.utils;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Created by azakhary on 6/23/2015.
 */
public class PhysicsDrag {

    public Actor draggingImage = null;

    /** ground body to connect the mouse joint to **/
    public Body groundBody;

    /** our mouse joint **/
    public MouseJoint mouseJoint = null;

    public PhysicsDrag(World world) {
        BodyDef bodyDef = new BodyDef();
        groundBody = world.createBody(bodyDef);
    }

}
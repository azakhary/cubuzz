package com.underwater.cubuzz.billing;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.underwater.cubuzz.Cubuzz;

/**
 * Created by sargis on 11/3/14.
 */
public class StoreListenerAdapter implements StoreListener {
    @Override
    public void productsReceived() {

    }

    @Override
    public void productsRequestFailed(Error error) {

    }

    @Override
    public void transactionCompleted(String productId) {
        awardProduct();
    }

    @Override
    public void transactionFailed(Error error) {

    }

    @Override
    public void transactionRestored(String productId) {
        awardProduct();
    }

    @Override
    public void transactionRestoreFailed(Error error) {

    }

    private void awardProduct(){
        Preferences prefs = Gdx.app.getPreferences(Cubuzz.PREFS);
        prefs.putBoolean(Cubuzz.NO_ADS_PRODUCT, true);
        prefs.flush();
    }
}

package com.underwater.cubuzz;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.underwater.cubuzz.ad.AdNetwork;
import com.underwater.cubuzz.billing.Store;
import com.underwater.cubuzz.i18n.LanguageManager;
import com.underwater.cubuzz.interaction.UserInteraction;
import com.underwater.cubuzz.scripts.data.CustomResourceManager;
import com.underwater.cubuzz.services.ServicesHelper;
import com.uwsoft.editor.renderer.data.ProjectInfoVO;
import com.uwsoft.editor.renderer.data.ResolutionEntryVO;
import com.uwsoft.editor.renderer.resources.IResourceRetriever;

import java.io.File;
import java.io.IOException;

/**
 * This is a main application listener of libGDX
 */
public class Cubuzz extends ApplicationAdapter {

	/**
	 * Constants for preferences (local data save storing) keys
	 */
	public static final String PREFS = "Cubuzz";
	public static final String PREFS_MAX_SCORE_KEY = "max_score";
	public static final String PREFS_SOUND_STATE_KEY = "sound_state";
	public static final String PREFS_NO_ADS = "no_ads";
	public static final String PREFS_SHOW_RATE_US = "show_rate_us";
	public static final String PREFS_GAME_RUN_COUNTER = "game_run_counter";
	public static final String PREFS_GAME_IN_PROGRESS = "game_in_progress";

	/**
	 * This is the number of game starts before we repeat rating dialog show
	 */
	public static final int RUNS_PER_RATING_REQUEST = 15;


	/**
	 * IAP Purchases Config
	 */
	public static final String NO_ADS_PRODUCT = "android.test.purchased";
	public static final String[] PRODUCT_ID_LIST = new String[]{
			NO_ADS_PRODUCT
	};

	/**
	 * Main managing class instances
	 */
	public LanguageManager languageManager;
	private CustomResourceManager rm;
	private MainStage stage;

	/**
	 * This variables are for device functionality,
	 * they will be null when running on desktop
	 */
	private Store store;
	private AdNetwork adNetwork;
	private ServicesHelper servicesHelper;

	/**
	 * User interaction is an interface to easily connect with third party functionality
	 */
	private UserInteraction userInteractionInterface;


	public ResolutionEntryVO resolution;

	@Override
	public void create () {
		// creating language manager
		languageManager = new LanguageManager();

		// Set language to load (this should be set to a value according to preferences if you have language selector)
		languageManager.setCurrentLanguage("ru");

		// Loading provided resolution data from o2d project
		rm = new CustomResourceManager();
		rm.setLanguageManager(languageManager);
		ProjectInfoVO projectInfoVo = rm.loadProjectVO();

		// currently supporting 2 different texture packs for big and small screens, for memory optimisation
		resolution = projectInfoVo.originalResolution;
		if(Gdx.graphics.getWidth() < 550) {
			resolution = projectInfoVo.getResolution("480x800");
		}
		rm.setWorkingResolution(resolution.name);
		rm.initAllResources();

		stage = new MainStage(this);

		/**
		 * initializing IAP's
		 */
		if(store != null) {
			store.initialize();
			store.requestProducts(PRODUCT_ID_LIST);
		}
	}

	public IResourceRetriever getRm() {
		return rm;
	}

	@Override
	public void render () {
		stage.act();
		Gdx.gl.glClearColor(1f, 253f / 255f, 229f / 255f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.draw();
	}

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public AdNetwork getAdNetwork() {
		return adNetwork;
	}

	public void setAdNetwork(AdNetwork adNetwork) {
		this.adNetwork = adNetwork;
	}

	public UserInteraction getUserInteraction() {
		return userInteractionInterface;
	}

	public void setInteractionInterface(UserInteraction userInteractionInterface) {
		this.userInteractionInterface = userInteractionInterface;
	}

	public ServicesHelper getServicesHelper() {
		return servicesHelper;
	}

	public void setServicesHelper(ServicesHelper servicesHelper) {
		this.servicesHelper = servicesHelper;
	}

	/**
	 * this returns file handle of a file in temp folder of current device, for storing the screenshot when required
	 * @return FileHandle
	 */
	public FileHandle getTmpFile() {
		if(userInteractionInterface != null) {
			File outputDir = userInteractionInterface.getCacheDir();
			try {
				File outputFile = File.createTempFile("screenshot", ".png", outputDir);
				return new FileHandle(outputFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			String screenshotName = "screenshot.png";
			FileHandle fh = new FileHandle(screenshotName);
			return fh;
		}

		return null;
	}
}
